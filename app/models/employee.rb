class Employee < ApplicationRecord
  belongs_to :company
  has_and_belongs_to_many :policies

  validates :name, presence: true
  validates :email, uniqueness: true

  acts_as_nested_set

  after_update :update_parent_child_count, :if => :parent_id_changed?



  def update_parent_child_count
    puts "=========="
    parent = Employee.find(self.parent_id) if self.parent_id.present?
    parent&.update(children_count: parent.children_count.to_i + 1)
  end
end
