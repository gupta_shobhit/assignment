class Company < ApplicationRecord
  require 'csv'
  has_many :employees
  has_many :policies
  validates :name, presence: true


  def read_sheet(url)
    puts "------"
    puts "#{url}"
    parent_child = Hash.new
    CSV.foreach(url, headers: true) do |row|
      employee = self.employees.where(email: row["Email"]).first_or_create!(name: row["Employee Name"], phone: row["Phone"])
      parent_child[employee] = row["Report To"]
      policies = row["Assigned Policies"].split("|")
      policies_ids = []
      policies.each do |p|
        policy = self.policies.where(name: p).first_or_create!
        policies_ids.push policy
      end
      employee.policies.destroy_all
      employee.policies<<policies_ids
    end
    msg = []
    parent_child.each do |key, value|
      if value.present?
        parent = Employee.find_by_email(value)
        parent.present? ? key.update!(parent_id: parent.id) : msg.push("Employee with email '#{value}' not exisits.")
      end
    end
    msg
  end
end
